import { AzureFunction, Context, HttpRequest } from '@azure/functions'

const orders = [
  {
    gl_dimension_pkids: '1001,1002',
    gl_dimension_code: 'XYZ,ABC',
    gl_code_reference: 'anything',
    gl_code: 'XYZ',
    item_code: 'X1000',
    item_type: 'INV',
    qty: 13,
    unit_price: 50
  },
  {
    gl_dimension_pkids: '1001,1002',
    gl_dimension_code: 'XYZ,ABC',
    gl_code_reference: 'anything',
    gl_code: 'XYZ',
    item_code: 'X1000',
    item_type: 'INV',
    qty: 10,
    unit_price: 20
  }
]

const run: AzureFunction = async (
  context: Context,
  req: HttpRequest
): Promise<void> => {
  context.log('HTTP trigger function processed a request.')
  const { orderId = undefined } = req.params

  if (!orderId) {
    context.res = {
      body: {
        orders
      }
    }
  } else if (orderId && orders[orderId]) {
    context.res = {
      // status: 200, /* Defaults to 200 */
      body: { order: orders[orderId] }
    }
  } else {
    context.res = {
      status: 404,
      body: {
        errors: {
          messages: [`Invalid orderId ${orderId}`]
        }
      }
    }
  }
}

export default run
