import { AzureFunction, Context, HttpRequest } from '@azure/functions'

const run: AzureFunction = async function(
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log('HTTP trigger function processed a request.')
  const name = req.query.name || (req.body && req.body.name)

  if (name) {
    context.res = {
      // status: 200, /* Defaults to 200 */
      body: { message: `Hello " + (req.query.name || req.body.name)` }
    }
  } else {
    context.res = {
      status: 400,
      body: {
        errors: {
          messages: [
            'Please pass a name on the query string or in the request body'
          ]
        }
      }
    }
  }
}

export default run
